#!/bin/bash
set -e
if ! which yarn > /dev/null ; then
    echo "yarn not installed. If you have root privilege, run"
    echo "npm i -g yarn"
    echo "Otherwise follow tutorial on https://github.com/sindresorhus/guides/blob/main/npm-global-without-sudo.md"
    exit 1
fi
# no check for git since error message will be clear enough

cd $(dirname "$0")/..

source ./config

echo "# init event repository"
mkdir repo
cd repo
# git clone https://gitlab.com/trichter/leipzig.git master
git clone https://gitlab.com/andrymi/radical-reykjavik.git master
cd master
echo "using git user ${USER} with email ${USER}@${HOST} but it doesn't really matter"
git config user.email "${USER}"'@'"${HOST}"
git config user.name "${USER}"
git config pull.rebase true

echo "========================"
echo "# init builder"
cd ../..
git clone https://gitlab.com/andrymi/builder.git
cd builder
yarn

echo "========================"
echo "# init scraper"
cd ..
git clone https://gitlab.com/andrymi/scraper.git
cd scraper
yarn
