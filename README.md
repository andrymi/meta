# trichter.cc
aggregating event calendar for Reykjavík ― in git

## features
- automated scraping of various sources (facebook, ical, ...)
- stores the events as markdown files
- generates HTML based on twig templates
- supports recurring events
- identifies and groups duplicated events
- concurrent access to branches possible through [git worktrees](https://spin.atomicobject.com/2016/06/26/parallelize-development-git-worktrees/)
- markdown rendering
- strongly typed ♥


## components
- [**andrymi/reykjavik**](https://gitlab.com/andrymi/reykjavik) \
  the events. in git! in Reykjavík!
- [**trichter/git-db**](https://gitlab.com/trichter/git-db)  [_@npm_](https://www.npmjs.com/package/@trichter/git-db) _a flat-file database based on git_ \
  abstracted base of the trichter database, for general use
- [**trichter/db**](https://gitlab.com/trichter/db) [_@npm_](https://www.npmjs.com/package/@trichter/db) \
  Database wich parses, validates and stores events, groups and their images
- [**trichter/scraper**](https://gitlab.com/andrymi/scraper) \
  scrapes events from various sources specified in the `group.md` definition files. (With Reykjavík time zone)
- [**trichter/builder**](https://gitlab.com/andrymi/builder) \
  generates static HTML based on the data. (With english translation and some Reykjaík references)
- [**trichter/logger**](https://gitlab.com/trichter/logger) [_@npm_](https://www.npmjs.com/package/@trichter/logger)  \
  basic console logging utility 
- **trichter/proxy** \
  closed source proxy utilizing some API keys combined with magic. write the maintainer!
All components from the amazing work of the [**trichter**](https://gitlab.com/trichter) maintainers. Thank you!


## installation
```sh
$ git clone https://gitlab.com/andrymi/meta trichter
$ cd trichter
$ ./scripts/init.sh
```

## usage
- `./scripts/validate.sh` \
  validates that every file in the database can be parsed correctly

- `./scripts/scrape.sh [filter]` \
   scrapes events based on the `source` specification in `group.md`-files.

   You can filter the groups that are scraped by passing a `filter` value.

- `./scripts/build.sh` \
  generates HTML based on the data and stores it into `./builder/output`

- `./scripts/serve.sh` \
  serves the generated page under [http://localhost:8000](http://localhost:8000)


## data structure
- every group gets its own folder
- name, website,... is defined in a `group.md` file
- group logo is set via a `group.png` or `group.jpg` image
- events are stored inside these group folders with a filename like `2019-10-05 Pretty Awesome Talk.md`
- recurring events can use underscore `_` instead of the date like `_ Magic Mondays.md`

#### `group.md` example
```yaml
---
name: Kanthaus
website: https://kanthaus.online
email: hello@kanthaus.online
address: Kantstraße 20, 04808 Wurzen
autocrawl:
  source: facebook # or ical
  options:
    page_id: kanthaus.online # like on facebook.com/kanthaus.online
--- 
Haus für nachhaltige Projekte # description
```


#### recurring event examples
_see  https://www.npmjs.com/package/moment-recur-ts#creating-rules for rule specifications_
#### every second week
```yaml
---
id: "1"
title: VoKü für Alle (vegan / vegetarisch)
start: '2018-06-24 20:00'
address: Kolonnadenstr. 19

recurring:
  rules:
    - units: 2
      measure: weeks
---               
```

##### every first monday per month
```yaml
---
id: "1"
title: Gesprächsangebot
start: '2019-11-04 19:00'
end: '2019-11-04 20:30'
address: Eisenbahnstrasse 125
link: https://outofaction.blackblogs.org/?page_id=142
teaser: "Wir bieten emotionale erste Hilfe für betroffene Einzelpersonen und Gruppen an"
recurring:
  rules:
    - units: 1
      measure: dayOfWeek
    - units: 0
      measure: weekOfMonthByDay
---
[Description...]
```

